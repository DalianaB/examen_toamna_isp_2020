package buciu.andreedaliana.exercitiul2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Aplicatie extends JFrame {
    JTextArea jTextArea1;
    JTextArea jTextArea2;
    JButton jButton;

    Aplicatie() {
        setTitle("Titlul ferestrei");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);
        initComponent();
        setVisible(true);
    }

    private void initComponent() {
        int width=300;int height = 20;

        jTextArea1 = new JTextArea("Se sterge si se scrie un alt text aici");
        jTextArea1.setBounds(40,50,width, height);
        jTextArea1.setEnabled(true);

        jTextArea2 = new JTextArea();
        jTextArea2.setBounds(40,100,width, height);
        jTextArea2.setEnabled(false);

        jButton = new JButton("Click");
        this.setLayout(null);
        jButton.setBounds(40,200, width,height);

        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    jTextArea2.setText(jTextArea1.getText());
                }catch (Exception evt) {
                    System.out.println(""+evt.getMessage());
                }
            }
        });
        add(jTextArea1);
        add(jTextArea2);
        add(jButton);


    }

}
